#include <iostream>
#include <cstdlib>

#include <TGame.h>
#include <TMainwindow.h>

using namespace std;

QApplication *_app;
TMainWindow  *_wnd;

int main(int argc, char* argv[]) {
    // TODO: debug output if application start in debug mode!!!
    // TODO: move draw borders in special method and call it after creating TScene()
    // TODO: improve GUI, recalculate block_h, block_w, after window resize

    // Создание Qt-приложения
    _app = new QApplication(argc, argv);

        // Создаем и показываем главное окно программы
        _wnd = new TMainWindow();
        _wnd->show();

    return (_app->exec());
}
